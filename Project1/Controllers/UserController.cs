﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SusceptibleApplication.Data;
using SusceptibleApplication.Helper;
using SusceptibleApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SusceptibleApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserDataHandler userHandler;
        private readonly CookieHelper cookieHelper;

        public UserController(IWebHostEnvironment host)
        {
            this.userHandler = new UserDataHandler(host.WebRootPath);
            this.cookieHelper = new CookieHelper();
        }

        

        [HttpGet]
        public IActionResult GetUser()
        {
            string session = Request.Cookies.SingleOrDefault(x => x.Key == "Sid").Value;
            var ip = HttpContext.Connection.RemoteIpAddress?.ToString();

            if (session == default)
            {
                return Unauthorized("User is not authenticated");
            }

            UserResponse response = new UserResponse();

            if(this.userHandler.IsExising(session, ip))
            {
                response.email = this.userHandler.GetUser(session).email;
            }
            else
            {
                return Unauthorized("User does not exist");
            }

            return Ok(response);
        }

        [HttpPost]
        public IActionResult Login(User user)
        {
            var ip = HttpContext.Connection.RemoteIpAddress?.ToString();

            if (!this.userHandler.IsExising(user))
            {
                return Unauthorized("User does not exist");
            }

            this.cookieHelper.SetSessionCookie(Response, user, this.userHandler, ip);


            return Ok();
        }

        [HttpGet("refresh")]
        public IActionResult RefreshUser()
        {
            string session = Request.Cookies.SingleOrDefault(x => x.Key == "Sid").Value;
            var ip = HttpContext.Connection.RemoteIpAddress?.ToString();
            if (session == default || !this.userHandler.IsExising(session, ip))
            {
                return Unauthorized("User is not authenticated");
            }

            var user = this.userHandler.GetUser(session);
            this.cookieHelper.SetSessionCookie(Response, user, this.userHandler, ip);

            return Ok();

        }

        [HttpDelete]
        public IActionResult DeleteSession()
        {
            string session = Request.Cookies.SingleOrDefault(x => x.Key == "Sid").Value;

            //this.userHandler.DeleteSession(session);

            return Ok();

        }
    }
}
