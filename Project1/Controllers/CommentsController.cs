﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SusceptibleApplication.Data;
using SusceptibleApplication.Helper;
using SusceptibleApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SusceptibleApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentDataHandler commentHandler;
        private readonly UserDataHandler userHandler;
        private readonly CookieHelper cookieHelper;

        public CommentsController(IWebHostEnvironment host)
        {
            this.commentHandler = new CommentDataHandler(host.WebRootPath);
            this.userHandler = new UserDataHandler(host.WebRootPath);
            this.cookieHelper = new CookieHelper();
        }

        [HttpGet]
        public IActionResult Get()
        {
            string session = Request.Cookies.SingleOrDefault(x => x.Key == "Sid").Value;
            var ip = HttpContext.Connection.RemoteIpAddress?.ToString();
            if (session == default || !this.userHandler.IsExising(session, ip))
            {
                return Unauthorized("User is not authenticated");
            }

            var comments = this.commentHandler.GetComments();

            var user = this.userHandler.GetUser(session);
            this.cookieHelper.SetSessionCookie(Response, user, this.userHandler, ip);

            return Ok(comments);
        }


        [HttpPost]
        public IActionResult Get(Comment comment)
        {
            string session = Request.Cookies.SingleOrDefault(x => x.Key == "Sid").Value;
            var ip = HttpContext.Connection.RemoteIpAddress?.ToString();
            if (session == default || !this.userHandler.IsExising(session, ip))
            {
                return Unauthorized("User is not authenticated");
            }

            this.commentHandler.SaveComment(comment);

            var user = this.userHandler.GetUser(session);
            this.cookieHelper.SetSessionCookie(Response, user, this.userHandler, ip);

            return Ok();
        }
    }
}
