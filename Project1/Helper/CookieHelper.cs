﻿using Microsoft.AspNetCore.Http;
using SusceptibleApplication.Data;
using SusceptibleApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SusceptibleApplication.Helper
{
    public class CookieHelper
    {

        public void SetSessionCookie(HttpResponse Response, User user, UserDataHandler userHandler, string ip)
        {
            var newSessionID = userHandler.CreateSession(user, ip);

            CookieOptions cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Secure = true,
                Expires = DateTime.Now.AddSeconds(62),
                SameSite = SameSiteMode.Strict
            };

            Response.Cookies.Append("Sid", newSessionID, cookieOptions);
        }

        public string CheckSessionCookie(HttpRequest request)
        {
            string session = request.Cookies.SingleOrDefault(x => x.Key == "Sid").Value;

            if (session == default)
            {
                throw new UnauthorizedAccessException("User is not authenticated");
            }

            return session;
        }
    }
}
