﻿using SusceptibleApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace SusceptibleApplication.Data
{
    public class CommentDataHandler
    {
        private string path;
        public CommentDataHandler(string hostpath)
        {
            this.path = Path.Combine(hostpath, "Users.xml");

            //If file does not exisit, create new and initialize
            if (!File.Exists(this.path))
            {
                using (File.Create(this.path)) { }
                XmlDocument doc = new XmlDocument();
                XmlElement element = doc.CreateElement("users");
                XmlElement element2 = doc.CreateElement("comments");
                XmlElement element3 = doc.CreateElement("data");
                element3.AppendChild(element2);
                element3.AppendChild(element);
                doc.Save(this.path);
            }
        }

        public List<Comment> GetComments()
        {
            List<Comment> comments = new List<Comment>();
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);

            var nodes = doc.SelectNodes("/data/comments/comment");

            foreach(XmlNode node in nodes)
            {
                Comment comment = new Comment();
                comment.email = node.SelectSingleNode("email").InnerText;
                comment.text = node.SelectSingleNode("text").InnerText;
                comments.Add(comment);
            }
            

            return comments;
        }

        public void SaveComment(Comment comment)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);
            var commentNodes = doc.SelectSingleNode("/data/comments");

            var commentElement = doc.CreateElement("comment");
            var emailElement = doc.CreateElement("email");
            var textElement = doc.CreateElement("text");
            emailElement.InnerText = comment.email;
            textElement.InnerText = comment.text;

            commentElement.AppendChild(emailElement);
            commentElement.AppendChild(textElement);

            commentNodes.AppendChild(commentElement);

            doc.Save(this.path);
        }
    }
}
