﻿using SusceptibleApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SusceptibleApplication.Data
{
    public class UserDataHandler
    {
        private string path;
        public UserDataHandler(string hostpath)
        {
            this.path = Path.Combine(hostpath, "Users.xml");

            //If file does not exisit, create new and initialize
            if (!File.Exists(this.path))
            {
                using (File.Create(this.path)) { }
                XmlDocument doc = new XmlDocument();
                XmlElement element = doc.CreateElement("users");
                XmlElement element2 = doc.CreateElement("comments");
                XmlElement element3 = doc.CreateElement("data");
                element3.AppendChild(element2);
                element3.AppendChild(element);
                doc.Save(this.path);
            }
        }

        public User GetUser(string session)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);

            var temp = doc.SelectSingleNode("/data/users/user[sessionID='" + session + "']");
            string email = temp.SelectSingleNode("email").InnerText;
            string password = temp.SelectSingleNode("password").InnerText;

            User user = new User()
            {
                email = email,
                password = password
            };

            return user;
        }

        public bool IsExising(User user)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);

            var node = doc.SelectSingleNode("/data/users/user[email='" + user.email + "'and password='" + user.password + "']");

            if(node == null)
            {
                return false;
            }

            return true;
        }

        public bool IsExising(string session, string ip)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);

            var node = doc.SelectSingleNode("/data/users/user[sessionID='" + session + "'and ip='" + ip + "']");

            if (node == null)
            {
                return false;
            }

            return true;
        }

        public void CreateUser(User user)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);
            var userNodes = doc.SelectSingleNode("/data/users");

            var userElement = doc.CreateElement("user");
            var emailElement = doc.CreateElement("email");
            emailElement.InnerText = user.email;
            var passwordElement = doc.CreateElement("password");
            passwordElement.InnerText = user.password;
            var ipElement = doc.CreateElement("ip");
            ipElement.InnerText = user.IP;

            userElement.AppendChild(emailElement);
            userElement.AppendChild(passwordElement);
            userElement.AppendChild(ipElement);

            userNodes.AppendChild(userElement);

            doc.Save(this.path);
        }

        public string CreateSession(User user, string ip)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);

            var parentNode = doc.SelectSingleNode("/data/users/user[email='" + user.email + "'and password='" + user.password + "']");
            var childNode = parentNode.SelectSingleNode("sessionID");
            var iPchildNode = parentNode.SelectSingleNode("ip");

            if (childNode != null)
            {
                parentNode.RemoveChild(childNode);
            }

            if (iPchildNode != null)
            {
                parentNode.RemoveChild(iPchildNode);
            }


            var session = computeSessionId(user);

            var sessionElement = doc.CreateElement("sessionID");
            sessionElement.InnerText = session;
            parentNode.AppendChild(sessionElement);

            var ipElement = doc.CreateElement("ip");
            ipElement.InnerText = ip;
            parentNode.AppendChild(ipElement);

            doc.Save(this.path);

            return session;
        }

        public void DeleteSession(string session)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.path);

            var parentNode = doc.SelectSingleNode("/data/users/user[sessionID='" + session + "']");

            if (parentNode != null)
            {
                var childNode = parentNode.SelectSingleNode("sessionID");

                if (childNode != null)
                {
                    parentNode.RemoveChild(childNode);
                }
            }

            doc.Save(this.path);
        }

        private string computeSessionId(User user)
        {
            SHA256 hash = SHA256.Create();
            RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
            byte[] randomBytes = new byte[32];

            var temp = "podf8and7sZ8b90oladn=dsilasdnf7nyl017lds7hlölsdfk3äsdfheabdklbakldowl" + DateTime.Now + user.email;          
            rngCsp.GetBytes(randomBytes);
            var tempBytes = Encoding.ASCII.GetBytes(temp);

            byte[] newArray = new byte[tempBytes.Length + randomBytes.Length];
            Array.Copy(tempBytes, newArray, tempBytes.Length);
            Array.Copy(randomBytes, 0, newArray, tempBytes.Length, randomBytes.Length);

            var sessBytes = hash.ComputeHash(newArray);
            var sessId = BitConverter.ToString(sessBytes).Replace("-", "");          

            return sessId;
        }
    }
}
