﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SusceptibleApplication.Models
{
    public class User
    {
        public string email { get; set; }
        public string password { get; set; }
        public string IP { get; set; }
    }
}
