import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/User';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  user: User = {
    email: "",
    password: ""
  };
  showWrongCredentials = false;

  constructor(private service: LoginService, public router: Router) { }

  ngOnInit() {
  }

  login() {
    this.service.login(this.user).subscribe(resp => {
      this.showWrongCredentials = false;
      this.router.navigate(['/']);
    }, error => {
        this.showWrongCredentials = true;
    });
  }

}
