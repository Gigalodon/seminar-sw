export interface Comment {
  email: string;
  text: string;
}
