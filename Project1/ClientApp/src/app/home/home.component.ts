import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { CommentService } from '../services/comment.service';
import { Comment } from '../models/Comment';
import { interval } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  comments: Comment[] = new Array;
  user = "";

  com: Comment = {
    email: "",
    text: ""
  }

  constructor(private commentService: CommentService, private authService: AuthenticationService, private router: Router) {

    interval(15000).subscribe(x => { // will execute every 30 seconds
      this.authService.refresh().subscribe(x => { }, x => {

        this.authService.delete().subscribe();       
        this.router.navigateByUrl("/login");

      });
    });
  }


  ngOnInit(): void {
    this.commentService.GetComments()
      .subscribe(resp => {
        resp.forEach(com => { this.comments.push(com) });
      });
    this.authService.isAuthenticated()
      .subscribe(resp => { this.com.email = resp.email }) 
  }

  Comment() {
    this.comments.push({ email: this.com.email, text: this.com.text });
    this.commentService.SendComment(this.com).subscribe();
  }



}
