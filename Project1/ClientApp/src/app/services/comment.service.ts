import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Comment } from '../models/Comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private client: HttpClient) { }

  GetComments() {
    return this.client.get<Comment[]>("/api/comments"); 
  }

  SendComment(comment: Comment) {
    return this.client.post("/api/comments", comment);
  }
}
