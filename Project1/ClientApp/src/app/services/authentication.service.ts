import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserResponse } from '../models/UserResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(public client: HttpClient) { }

  public isAuthenticated() {
    return this.client.get<UserResponse>("/api/user");
  }

  public refresh() {
    return this.client.get("/api/user/refresh");
  }

  public delete() {
    return this.client.delete("/api/user");
  }
}
